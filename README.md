# Challenge Week 11 - Unit Test

[![pipeline status](https://gitlab.com/arifintahu/msib-challenge-week-11/badges/main/pipeline.svg)](https://gitlab.com/arifintahu/msib-challenge-week-11/-/commits/main)
[![coverage report](https://gitlab.com/arifintahu/msib-challenge-week-11/badges/main/coverage.svg)](https://gitlab.com/arifintahu/msib-challenge-week-11/-/commits/main)

In this challenge, we have a movie reviewer API with modular monolith design pattern. This pattern organizes an application into separate, independently manageable modules, enhancing code reusability and maintainability. This project also have docker compose configuration and gitlab ci for CI/CD unit tests.

This project have three modules: `movie`, `review`, and `user` module. Your task is to complete the module `review` unit test on the repositories folder and services folder.

## Objectives

1. Write `ReviewRepository` unit test and its mock resources, [TODO](https://gitlab.com/arifintahu/msib-challenge-week-11/-/blob/main/src/modules/review/repositories/test/review.repository.spec.js?ref_type=heads#L5)
2. Write `ReviewService` unit test and its mock resources, [TODO](https://gitlab.com/arifintahu/msib-challenge-week-11/-/blob/main/src/modules/review/services/test/review.service.spec.js?ref_type=heads#L5)

## Postman Collection

```
https://api.postman.com/collections/23885186-2e9ace31-2178-4aa5-bbe4-230742bd2151?access_key=PMAT-01HD1223JKXT8C8H3KEJ1XMK5C
```

## How to Submit

1. Fork this [repository](https://gitlab.com/arifintahu/msib-challenge-week-11)
2. Clone forked repository
3. Install dependencies: `npm install`
4. Run husky preparation: `npm run prepare`
5. Copy `.env.example` to `.env`
6. Complete the objectives
7. Run test by `npm test`
8. Commit and push your changes
9. Create a Pull Request to original repository

## Commands

-   Run server using docker

    ```
    docker-compose up
    ```

-   Run unit test with coverages
    ```
    npm test
    ```
