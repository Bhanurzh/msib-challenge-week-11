const ReviewRepository = require('..//review.repository');
const Review = require('../../../../shared/database/models/review.model');
const mockResources = require('./mockResources');

// TODO: write ReviewRepository unit test
describe('ReviewRepository', () => {
    describe('ReviewRepository.__createReview', () => {
        it('should return review created', async () => {
            // arrange
            const mockInput =
                mockResources.ReviewRepository.createReview.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.ReviewRepository.createReview
                    .POSITIVE_CASE_OUTPUT;

            Review.create = jest.fn().mockResolvedValue(mockOutput);

            // act
            const result = await new ReviewRepository().createReview(mockInput);

            //assert
            expect(result).toEqual(mockOutput);
            expect(Review.create).toHaveBeenCalledTimes(1);
            expect(Review.create).toBeCalledWith(mockInput);
        });
    });

    describe('ReviewRepository.__getMovieReviews', () => {
        it('should return list movie reviews', async () => {
            //arrange
            const mockInput =
                mockResources.ReviewRepository.getMovieReviews
                    .POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.ReviewRepository.getMovieReviews
                    .POSITIVE_CASE_OUTPUT;
            const paramsFindAll =
                mockResources.ReviewRepository.getMovieReviews.PARAMS_FIND_ALL;

            Review.findAll = jest.fn().mockResolvedValue(mockOutput);

            //act
            const result = await new ReviewRepository().getMovieReviews(
                mockInput
            );

            //assert
            expect(result).toEqual(mockOutput);
            expect(Review.findAll).toHaveBeenCalledTimes(1);
            expect(Review.findAll).toBeCalledWith(paramsFindAll);
        });
    });

    describe('ReviewRepository.__getReviewByUserIdAndMovieId', () => {
        it('should return review by userId and movieId', async () => {
            //arrange
            const mockInput =
                mockResources.ReviewRepository.getReviewByUserIdAndMovieId
                    .POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.ReviewRepository.getReviewByUserIdAndMovieId
                    .POSITIVE_CASE_OUTPUT;
            const paramsFindOne =
                mockResources.ReviewRepository.getReviewByUserIdAndMovieId
                    .PARAMS_FIND_ONE;

            Review.findOne = jest.fn().mockResolvedValue(mockOutput);

            //act
            const result =
                await new ReviewRepository().getReviewByUserIdAndMovieId(
                    mockInput.userId,
                    mockInput.movieId
                );

            //assert
            expect(result).toEqual(mockOutput);
            expect(Review.findOne).toHaveBeenCalledTimes(1);
            expect(Review.findOne).toBeCalledWith(paramsFindOne);
        });
    });
});
