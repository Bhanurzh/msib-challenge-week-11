const { User, Movie } = require('../../../../shared/database/models');

// TODO: write mock resources for ReviewRepository test
const mockResources = {
    ReviewRepository: {
        createReview: {
            POSITIVE_CASE_INPUT: {
                userId: 1,
                movieId: 1,
                rating: 5,
                comment: 'Good'
            },
            POSITIVE_CASE_OUTPUT: {
                id: 1,
                userId: 1,
                movieId: 1,
                rating: 5,
                comment: 'Good',
                createdAt: '2022-08-26 14:40:19',
                updatedAt: '2022-08-26 14:40:19'
            }
        },
        getMovieReviews: {
            POSITIVE_CASE_INPUT: {
                movieId: 1,
                page: 1,
                perPage: 10
            },
            POSITIVE_CASE_OUTPUT: [
                {
                    id: 1,
                    userId: 1,
                    movieId: 1,
                    rating: 5,
                    comment: 'Good',
                    userEmail: 'isma123@gmail.com',
                    movieTitle: 'Batman'
                }
            ],
            PARAMS_FIND_ALL: {
                attributes: [
                    'id',
                    'userId',
                    'movieId',
                    'rating',
                    'comment',
                    [{ col: 'User.email' }, 'userEmail'],
                    [{ col: 'Movie.title' }, 'movieTitle']
                ],
                include: [
                    {
                        attributes: [],
                        model: User
                    },
                    {
                        attributes: [],
                        model: Movie
                    }
                ],
                where: {
                    movieId: 1
                },
                limit: 10,
                offset: 0
            }
        },
        getReviewByUserIdAndMovieId: {
            POSITIVE_CASE_INPUT: {
                userId: 1,
                movieId: 1
            },
            POSITIVE_CASE_OUTPUT: {
                id: 1,
                userId: 1,
                movieId: 1,
                rating: 5,
                comment: 'Good',
                createdAt: '2022-08-26 14:40:19',
                updatedAt: '2022-08-26 14:40:19'
            },
            PARAMS_FIND_ONE: {
                where: {
                    userId: 1,
                    movieId: 1
                }
            }
        }
    }
};

module.exports = mockResources;
