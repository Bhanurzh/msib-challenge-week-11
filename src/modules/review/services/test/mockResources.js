// TODO: write mock resources for ReviewService test

const mockResources = {
    ReviewService: {
        createReview: {
            POSITIVE_CASE_INPUT_USER: {
                userId: 1
            },
            POSITIVE_CASE_INPUT_MOVIE: {
                movieId: 1
            },
            POSITIVE_CASE_INPUT_GET_REVIEW: {
                userId: 1,
                movieId: 1
            },
            POSITIVE_CASE_INPUT_CREATE_REVIEW: {
                userId: 1,
                movieId: 1,
                rating: 5,
                comment: 'Good'
            },
            POSITIVE_CASE_OUTPUT_USER: {
                id: 1,
                email: 'isma123@gmail.com',
                role: 'General'
            },
            POSITIVE_CASE_OUTPUT_MOVIE: {
                id: 1,
                title: 'Batman',
                year: 2020,
                genre: 'action',
                language: 'English',
                createdAt: '2022-08-26 14:40:19',
                updatedAt: '2022-08-26 14:40:19'
            },
            POSITIVE_CASE_OUTPUT_GET_REVIEW: {
                id: 1,
                userId: 1,
                movieId: 1,
                rating: 5,
                comment: 'Good',
                createdAt: '2022-08-26 14:40:19',
                updatedAt: '2022-08-26 14:40:19'
            },
            POSITIVE_CASE_OUTPUT_CREATE_REVIEW: {
                id: 1,
                userId: 1,
                movieId: 1,
                rating: 5,
                comment: 'Good',
                createdAt: '2022-08-26 14:40:19',
                updatedAt: '2022-08-26 14:40:19'
            }
        },
        getMovieReviews: {
            POSITIVE_CASE_INPUT: {
                movieId: 1,
                page: 1,
                perPage: 10
            },
            POSITIVE_CASE_INPUT_MOVIE: {
                movieId: 1
            },
            POSITIVE_CASE_OUTPUT: [
                {
                    id: 1,
                    userId: 1,
                    movieId: 1,
                    rating: 5,
                    comment: 'Good',
                    userEmail: 'isma123@gmail.com',
                    movieTitle: 'Batman'
                }
            ],
            POSITIVE_CASE_OUTPUT_MOVIE: {
                id: 1,
                title: 'Batman',
                year: 2020,
                genre: 'action',
                language: 'English',
                createdAt: '2022-08-26 14:40:19',
                updatedAt: '2022-08-26 14:40:19'
            }
        }
    }
};

module.exports = mockResources;
