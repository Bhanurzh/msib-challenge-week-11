const mockResources = require('./mockResources');
const ReviewRepository = require('../../repositories/review.repository');
const MovieRepository = require('../../../movie/repositories/movie.repository');
const MovieService = require('../../../movie/services/movie.service');
const UserRepository = require('../../../user/repositories/user.repository');
const UserService = require('../../../user/services/user.service');

// TODO: write ReviewService unit test
describe('ReviewService', () => {
    describe('ReviewService.__createReview', () => {
        it('should return review created', async () => {
            // arrange
            const mockInputUser =
                mockResources.ReviewService.createReview
                    .POSITIVE_CASE_INPUT_USER;
            const mockOutputUser =
                mockResources.ReviewService.createReview
                    .POSITIVE_CASE_OUTPUT_USER;
            const mockInputMovie =
                mockResources.ReviewService.createReview
                    .POSITIVE_CASE_INPUT_MOVIE;
            const mockOutputMovie =
                mockResources.ReviewService.createReview
                    .POSITIVE_CASE_OUTPUT_MOVIE;
            const mockInputGetReview =
                mockResources.ReviewService.createReview
                    .POSITIVE_CASE_INPUT_GET_REVIEW;
            const mockOutputGetReview =
                mockResources.ReviewService.createReview
                    .POSITIVE_CASE_OUTPUT_GET_REVIEW;
            const mockInputCreateReview =
                mockResources.ReviewService.createReview
                    .POSITIVE_CASE_INPUT_CREATE_REVIEW;
            const mockOutputCreateReview =
                mockResources.ReviewService.createReview
                    .POSITIVE_CASE_OUTPUT_CREATE_REVIEW;

            const userRepository = new UserRepository();
            userRepository.getUserById = jest
                .fn()
                .mockResolvedValue(mockOutputUser);

            const movieRepository = new MovieRepository();
            movieRepository.getMovieById = jest
                .fn()
                .mockResolvedValue(mockOutputMovie);

            const reviewRepository = new ReviewRepository();

            reviewRepository.getReviewByUserIdAndMovieId = jest
                .fn()
                .mockResolvedValue(mockOutputGetReview);

            reviewRepository.createReview = jest
                .fn()
                .mockResolvedValue(mockOutputCreateReview);

            // act
            const resultUser = await new UserService(
                userRepository
            ).getUserById(mockInputUser.userId);

            const resultMovie = await new MovieService(
                movieRepository
            ).getMovieById(mockInputMovie.movieId);

            const resultGetReview =
                await reviewRepository.getReviewByUserIdAndMovieId(
                    mockInputGetReview
                );

            const resultCreateReview = await reviewRepository.createReview(
                mockInputCreateReview
            );

            // assert
            expect(resultUser).toEqual(mockOutputUser);
            expect(userRepository.getUserById).toHaveBeenCalledTimes(1);
            expect(userRepository.getUserById).toBeCalledWith(
                mockInputUser.userId
            );

            expect(resultMovie).toEqual(mockOutputMovie);
            expect(movieRepository.getMovieById).toHaveBeenCalledTimes(1);
            expect(movieRepository.getMovieById).toBeCalledWith(
                mockInputMovie.movieId
            );

            expect(resultGetReview).toEqual(mockOutputGetReview);
            expect(
                reviewRepository.getReviewByUserIdAndMovieId
            ).toHaveBeenCalledTimes(1);
            expect(reviewRepository.getReviewByUserIdAndMovieId).toBeCalledWith(
                mockInputGetReview
            );

            expect(resultCreateReview).toEqual(mockOutputCreateReview);
            expect(reviewRepository.createReview).toHaveBeenCalledTimes(1);
            expect(reviewRepository.createReview).toBeCalledWith(
                mockInputCreateReview
            );
        });
    });

    describe('ReviewService.__getMovieReviews', () => {
        it('should return list movie reviews', async () => {
            // arrange
            const mockInputMovie =
                mockResources.ReviewService.getMovieReviews
                    .POSITIVE_CASE_INPUT_MOVIE;
            const mockOutputMovie =
                mockResources.ReviewService.getMovieReviews
                    .POSITIVE_CASE_OUTPUT_MOVIE;
            const mockInputReview =
                mockResources.ReviewService.getMovieReviews.POSITIVE_CASE_INPUT;
            const mockOutputReview =
                mockResources.ReviewService.getMovieReviews
                    .POSITIVE_CASE_OUTPUT;

            const movieRepository = new MovieRepository();
            movieRepository.getMovieById = jest
                .fn()
                .mockResolvedValue(mockOutputMovie);

            const reviewRepository = new ReviewRepository();
            reviewRepository.getMovieReviews = jest
                .fn()
                .mockResolvedValue(mockOutputReview);

            // act
            const resultMovie = await new MovieService(
                movieRepository
            ).getMovieById(mockInputMovie.movieId);

            const resultReview =
                await reviewRepository.getMovieReviews(mockInputReview);

            // assert
            expect(resultMovie).toEqual(mockOutputMovie);
            expect(movieRepository.getMovieById).toHaveBeenCalledTimes(1);
            expect(movieRepository.getMovieById).toBeCalledWith(
                mockInputMovie.movieId
            );

            expect(resultReview).toEqual(mockOutputReview);
            expect(reviewRepository.getMovieReviews).toHaveBeenCalledTimes(1);
            expect(reviewRepository.getMovieReviews).toBeCalledWith(
                mockInputReview
            );
        });
    });
});
