const mockResources = {
    MovieService: {
        createMovie: {
            POSITIVE_CASE_INPUT: {
                title: 'Batman',
                year: 2020,
                genre: 'action',
                language: 'English'
            },
            POSITIVE_CASE_OUTPUT: {
                id: 1,
                title: 'Batman',
                year: 2020,
                genre: 'action',
                language: 'English',
                createdAt: '2022-08-26 14:40:19',
                updatedAt: '2022-08-26 14:40:19'
            }
        },
        getMovies: {
            POSITIVE_CASE_INPUT: {
                page: 1,
                perPage: 10
            },
            POSITIVE_CASE_OUTPUT: [
                {
                    id: 1,
                    title: 'Batman',
                    year: 2020,
                    genre: 'action',
                    language: 'English'
                }
            ]
        },
        getMovieById: {
            POSITIVE_CASE_INPUT: {
                id: 1
            },
            POSITIVE_CASE_OUTPUT: {
                id: 1,
                title: 'Batman',
                year: 2020,
                genre: 'action',
                language: 'English',
                createdAt: '2022-08-26 14:40:19',
                updatedAt: '2022-08-26 14:40:19'
            }
        }
    }
};

module.exports = mockResources;
