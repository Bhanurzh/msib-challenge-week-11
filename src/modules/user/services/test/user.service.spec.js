const UserService = require('../user.service');
const UserRepository = require('../../repositories/user.repository');
const AuthService = require('../auth.service');
const PasswordManager = require('../password-manager');
const mockResources = require('./mockResources');

describe('UserService', () => {
    describe('UserService.__register', () => {
        it('should return registered user', async () => {
            //arrange
            const mockInput =
                mockResources.UserService.register.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.UserService.register.POSITIVE_CASE_OUTPUT;

            const userRepository = new UserRepository();
            userRepository.getUserByEmail = jest
                .fn()
                .mockResolvedValue(
                    mockResources.UserService.register
                        .POSITIVE_CASE_GET_USER_BY_EMAIL
                );
            userRepository.createUser = jest.fn().mockResolvedValue(mockOutput);

            const passwordManager = new PasswordManager();
            passwordManager.hashPassword = jest
                .fn()
                .mockResolvedValue(mockInput.hashedPassword);

            //act
            const result = await new UserService(
                userRepository,
                null,
                passwordManager
            ).register({
                email: mockInput.email,
                password: mockInput.password,
                role: mockInput.role
            });

            //assert
            expect(result).toEqual(mockOutput);
            expect(userRepository.getUserByEmail).toHaveBeenCalledTimes(1);
            expect(userRepository.getUserByEmail).toBeCalledWith(
                mockInput.email
            );

            expect(passwordManager.hashPassword).toHaveBeenCalledTimes(1);
            expect(passwordManager.hashPassword).toBeCalledWith(
                mockInput.password
            );

            expect(userRepository.createUser).toHaveBeenCalledTimes(1);
            expect(userRepository.createUser).toBeCalledWith({
                email: mockInput.email,
                password: mockInput.hashedPassword,
                role: mockInput.role
            });
        });
    });
});
