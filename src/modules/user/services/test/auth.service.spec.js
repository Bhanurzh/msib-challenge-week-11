const jwt = require('jsonwebtoken');
const AuthService = require('../auth.service');
const mockResources = require('./mockResources');
const { addMinutes } = require('date-fns');

jest.mock('jsonwebtoken');

describe('AuthService', () => {
    describe('AuthService.__createToken', () => {
        it('should return token', () => {
            //arrange
            const mockInput =
                mockResources.AuthService.createToken.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.AuthService.createToken.POSITIVE_CASE_OUTPUT;

            jwt.sign.mockReturnValue(mockOutput);

            //act
            const { expiry, secretKey } = mockResources.AuthService;
            const result = new AuthService(expiry, secretKey).createToken(
                mockInput.id
            );

            //assert
            expect(result).toEqual(mockOutput);
            expect(jwt.sign).toHaveBeenCalledTimes(1);

            const expiresIn = Math.floor(
                addMinutes(new Date(), expiry).getTime() / 1000
            );
            const authUser = { id: mockInput.id, exp: expiresIn };
            expect(jwt.sign).toBeCalledWith(authUser, secretKey);
        });
    });
});
