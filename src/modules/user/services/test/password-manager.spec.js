const bcryptjs = require('bcryptjs');
const PasswordManager = require('../password-manager');
const mockResources = require('./mockResources');

jest.mock('bcryptjs');

describe('PasswordManager', () => {
    describe('PasswordManager.__hashPassword', () => {
        it('should return hashed password', async () => {
            //arrange
            const mockInput =
                mockResources.PasswordManager.hashPassword.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.PasswordManager.hashPassword.POSITIVE_CASE_OUTPUT;

            bcryptjs.hash.mockResolvedValue(mockOutput.hashedPassword);

            //act
            const result = await new PasswordManager().hashPassword(
                mockInput.password
            );

            //assert
            expect(result).toEqual(mockOutput.hashedPassword);
            expect(bcryptjs.hash).toHaveBeenCalledTimes(1);
            expect(bcryptjs.hash).toBeCalledWith(mockInput.password, 12);
        });
    });

    describe('PasswordManager.__compare', () => {
        it('should return boolean compare password and hashed password', async () => {
            //arrange
            const mockInput =
                mockResources.PasswordManager.compare.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.PasswordManager.compare.POSITIVE_CASE_OUTPUT;

            bcryptjs.compare.mockResolvedValue(mockOutput);

            //act
            const result = await new PasswordManager().compare(
                mockInput.password,
                mockInput.hashedPassword
            );

            //assert
            expect(result).toEqual(mockOutput);
            expect(bcryptjs.compare).toHaveBeenCalledTimes(1);
            expect(bcryptjs.compare).toBeCalledWith(
                mockInput.password,
                mockInput.hashedPassword
            );
        });
    });
});
